# README #

This is where you will find my most current CV.

### What is this repository for? ###

* Recruitment agencies and employers looking for a mid/senior IT professional.
* I prefer to work within the East Midlands (UK) but would consider roles outside of this region.
* https://www.linkedin.com/in/dennisfurr/

### About me ###

* US Navy veteran (10 years)
* Immigrated to the UK in 1999
* Married with an adult child
* Lives aboard a narrowboat (2nd home) currently moored at Sawley Marina
* Owns a home in Nottingham
* Full UK drivers license and vehicle